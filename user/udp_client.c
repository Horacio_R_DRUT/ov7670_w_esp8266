/******************************************************************************
 * Copyright 2013-2014 Espressif Systems
 *
 *******************************************************************************/
#include "ets_sys.h"
#include "os_type.h"
#include "osapi.h"
#include "mem.h"
#include "user_interface.h"
#include "user_config.h"
#include "user_camera.h"
#include "user_gpio.h"

#include "espconn.h"

LOCAL os_timer_t test_timer;
LOCAL struct espconn user_udp_espconn;
uint8 DeviceBuffer[(PACKET_SIZE*H_RESOL)+1];
uint16 i=0;
uint16 k=0;

/*---------------------------------------------------------------------------*/
LOCAL struct espconn ptrespconn;

/******************************************************************************
 * FunctionName : user_udp_recv_cb
 * Description  : Processing the received udp packet
 * Parameters   : arg -- Additional argument to pass to the callback function
 *                pusrdata -- The received data (or NULL when the connection has been closed!)
 *                length -- The length of received data
 * Returns      : none
 *******************************************************************************/
LOCAL void ICACHE_FLASH_ATTR
user_udp_recv_cb(void *arg, char *pusrdata, unsigned short length)
{
	ets_uart_printf("recv udp data: %s\n", pusrdata);
}

/******************************************************************************
 * FunctionName : user_udp_send
 * Description  : udp send data
 * Parameters   : data to be sent
 * Returns      : none
 *******************************************************************************/
void ICACHE_FLASH_ATTR
user_udp_send(void)
{
	struct ip_info ipconfig;
	const char udp_remote_ip[4] = {192, 168, 1, 65};
	os_memcpy(user_udp_espconn.proto.udp->remote_ip, udp_remote_ip, 4); // ESP8266 udp remote IP need to be set everytime we call espconn_sent
	user_udp_espconn.proto.udp->remote_port = 8100;  // ESP8266 udp remote port need to be set everytime we call espconn_sent


	for (i=0; i<PACKET_SIZE*H_RESOL; i++) {
		DeviceBuffer[i]= line[i];
		if (TESTING>2)	ets_uart_printf("line int: %d\n", DeviceBuffer[i]);
	}


	if (DevBuffer_length != 0) {
		if (TESTING>1)	ets_uart_printf("sending line %d! \n", k);
		if (k==0) {
			// Add start-of-frame marker.
			ets_uart_printf("adding SOF. \n");
			int res = espconn_send(&user_udp_espconn, DeviceBuffer, DevBuffer_length);
		}
		else {
			espconn_send(&user_udp_espconn, DeviceBuffer, DevBuffer_length-1);
		}
	}
}

/******************************************************************************
 * FunctionName : user_udp_sent_cb
 * Description  : udp sent successfully
 * Parameters  : arg -- Additional argument to pass to the callback function
 * Returns      : none
 *******************************************************************************/
LOCAL void ICACHE_FLASH_ATTR
user_udp_sent_cb(void *arg)
{
	struct espconn *pespconn = arg;
	if (TESTING>1)	ets_uart_printf("user_udp_send successfully !!!\n");
	k++;
	if (k==V_RESOL/PACKET_SIZE) {
		DevBuffer_length = 0;
		k = 0;
		//os_delay_us(10000);
		if (TESTING>1)	ets_uart_printf("Frame sent !!!\n");
		ETS_GPIO_INTR_ENABLE();  // Re enable interrupts when finished sending.
	}
	else {
		ETS_GPIO_INTR_ENABLE();  // Re enable interrupts when finished sending.
//		//disarm timer first
//		os_timer_disarm(&test_timer);
//		//re-arm timer
//		os_timer_setfn(&test_timer, (os_timer_func_t *)user_udp_send, NULL); // only send next packet after prev packet sent successfully
//		os_timer_arm(&test_timer, 10, 0);
	}
}


/******************************************************************************
 * FunctionName : user_check_ip
 * Description  : check whether get ip addr or not
 * Parameters   : none
 * Returns      : none
 *******************************************************************************/
void ICACHE_FLASH_ATTR
user_check_ip(void)
{
	struct ip_info ipconfig;

	//disarm timer first
	os_timer_disarm(&test_timer);

	//get ip info of ESP8266 station
	wifi_get_ip_info(STATION_IF, &ipconfig);

	if (wifi_station_get_connect_status() == STATION_GOT_IP && ipconfig.ip.addr != 0)
	{
		ets_uart_printf("got ip !!! \r\n");

		wifi_set_broadcast_if(STATION_MODE); // send UDP broadcast from station interface

		user_udp_espconn.type = ESPCONN_UDP;
		user_udp_espconn.proto.udp = (esp_udp *)os_zalloc(sizeof(esp_udp));
		user_udp_espconn.proto.udp->local_port = espconn_port();  // get an available port

		const char udp_remote_ip[4] = {192, 168, 1, 65};

		os_memcpy(user_udp_espconn.proto.udp->remote_ip, udp_remote_ip, 4); // ESP8266 udp remote IP

		user_udp_espconn.proto.udp->remote_port = 8100;  // ESP8266 udp remote port

		espconn_regist_recvcb(&user_udp_espconn, user_udp_recv_cb); // register a udp packet receiving callback
		espconn_regist_sentcb(&user_udp_espconn, user_udp_sent_cb); // register a udp packet sent callback

		espconn_create(&user_udp_espconn);   // create udp connection

		// Initialise camera via i2c interface.
		while(camera_init()!=1);

	}
	else
	{
		if ((wifi_station_get_connect_status() == STATION_WRONG_PASSWORD ||
				wifi_station_get_connect_status() == STATION_NO_AP_FOUND ||
				wifi_station_get_connect_status() == STATION_CONNECT_FAIL))
		{
			ets_uart_printf("connect fail !!! \r\n");
		}
		else
		{
			//re-arm timer to check ip
			os_timer_setfn(&test_timer, (os_timer_func_t *)user_check_ip, NULL);
			os_timer_arm(&test_timer, 500, 0);
		}
	}
}


/******************************************************************************
 * FunctionName : user_set_station_config
 * Description  : set the router info which ESP8266 station will connect to
 * Parameters   : none
 * Returns      : none
 *******************************************************************************/
void ICACHE_FLASH_ATTR
user_set_station_config(void)
{
	// Wifi configuration
	char ssid[32] = STATION_SSID;
	char password[64] = STATION_PASSWORD;
	struct station_config stationConf;
	struct ip_info info;

	//need not mac address
	stationConf.bssid_set = 0;

	//Set ap settings
	os_memcpy(&stationConf.ssid, ssid, 32);
	os_memcpy(&stationConf.password, password, 64);


    wifi_get_ip_info(STATION_IF, &info);
	char *dhcp = "static";
	char *ip, *mask, *gw;
	ip_addr_t dns;
	if (!dhcp || strcmp(dhcp, "dhcp") != 0)
	{
		ip = AP_STATIC_IP;
		mask = "255.255.255.0";
		gw = "192.168.1.1";
		IP4_ADDR(&dns, 192, 168, 1, 1);

		if (ip)
			info.ip.addr = ipaddr_addr(ip);
		if (mask)
			info.netmask.addr = ipaddr_addr(mask);
		if (gw)
			info.gw.addr = ipaddr_addr(gw);

		espconn_dns_setserver(1, &dns);
		wifi_station_dhcpc_stop();
		wifi_set_ip_info(STATION_IF, &info);
	}

	while(!wifi_station_set_config(&stationConf));

	//set a timer to check whether got ip from router succeeds or not.
	os_timer_disarm(&test_timer);
	os_timer_setfn(&test_timer, (os_timer_func_t *)user_check_ip, NULL);
	os_timer_arm(&test_timer, 1000, 0);

	wifi_station_set_auto_connect(TRUE);
	wifi_station_connect();
}


/******************************************************************************
 * FunctionName : user_init
 * Description  : entry of user application, init user function here
 * Parameters   : none
 * Returns      : none
 *******************************************************************************/
void udp_init(void)
{
	ets_uart_printf("SDK version:%s\n", system_get_sdk_version());

	wifi_set_opmode(STATION_MODE);
	//Config connection and link to router...
	user_set_station_config();

}
